#importe les différents modules dont nous avons besoin
import pandas as pd
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter import ttk
import matplotlib.pyplot as plt
from datetime import datetime

################################################################# dictionnaires avec le nombre de jours par mois #########################################################################################

moiss = {'01':31, '02':28, '03':31, '04':30, '05':31, '06':30, '07':31, '08':31, '09':30, '10':31, '11':30, '12':31}      #format d'une année "classique"
mois_bissextile = {'01':31, '02':29, '03':31, '04':30, '05':31, '06':30, '07':31, '08':31, '09':30, '10':31, '11':30, '12':31}      #format d'une année bissextile

############################################################### FONCTIONS POUR UNE SEULE PERIODE DE TEMPS #######################################################################

def trouver_ligne(date):                                ##trouve la ligne correspondante à une date au format AAAMMJJ
    ligne = df.index[df['AAAAMMJJ'] == date].tolist()       #trouvé sur internet / donne la ligne dans le fichier d'une date saison
    return ligne[0] + 2                                     #retourne la ligne de la date

def bissextile(date):                                   ## fonctionne avec une année ou avec une date format AAAAMMJJ
    if date % 4 == 0:                                       #Si l'année modulo 4 = 0
        return True                                         #L'année est bissextile
    else:                                                   #Sinon
        return False                                        #L'année n'est pas bissextile

def cumul_pluviométrique_jour(date):                   ##Donne la valeur d'un jour
    valeur =(df['RR'][trouver_ligne(date)].tolist())        #trouve la valeur
    return valeur                                           #renvoie la valeur

def cumul_pluviometrique_mois(date):                    ##Donne le cumul d'un mois
    cumul = 0                                               #définit la variable qui contiendras le cumul
    month = str(date)[4:6]                                  #récupère le numéro du mois
    year = str(date)[0:4]                                   #récupère le numéro de l'année
    year = int(year)                                        #reconvertit l'année en nombre entier pour faire des opérations avec
    if year % 4 == 0:                                       #vérifie si l'année est bissextile
        jours = mois_bissextile[month]                      #utilise le format des années bissextiles
    else:                                                   #sinon
        jours = moiss[month]                                #utilise le format des années "classiques"
    for i in range(jours):                                  #répéter pour le nombre de jour dans le mois
        cumul += cumul_pluviométrique_jour(date)            #additione la valeur du jour traiter actuellement
        date += 1                                           #passe au jour d'après
    return cumul                                            #retourne la valeur totale

def cumul_pluvimetrique_annee(date):                    ##effectue la somme des valeurs pluviométriques d'une année
    cumul=0                                                 #définit la variable qui contiendras le cumul
    for i in range(12):                                     #pour chaque mois de l'année
        cumul+=cumul_pluviometrique_mois(date)              #fais le cumul du mois
        date+=100                                           #passe au mois suivant
    return cumul                                            #retourne la valeur totale de l'année

def cumul_pluviometrique_saison(date):                  ##effectue la somme des valeurs d'une saison
    cumul=0                                                 #définit la variable qui contiendras le cumul
    month = str(date)[4:6]                                  #récupère le mois de départ de la saison
    if month == '12':                                       #si le mois est décembre (hiver a cheval sur deux ans en météorologie)
        cumul+=cumul_pluviometrique_mois(date)              #cumul de décembre
        date+=8900                                          #passage a janvier de l'année d'après
        cumul+=cumul_pluviometrique_mois(date)              #cumul de janvier
        date+=100                                           #passage à février
        cumul+=cumul_pluviometrique_mois(date)              #cumul de février
    else:                                                   #sinon
        for i in range(3):                                  #pour chacun des 3 mois de la saison
            cumul+=cumul_pluviometrique_mois(date)          #cumul du mois
            date+=100                                       #passage au mois suivant
    return cumul                                            #retourne le cumul de la saison

def cumul_pluviometrique_decade(date):                  ##effectue la somme des valeurs d'une décade
    cumul=0                                                 #définit la variable qui contiendras le cumul
    decade = str(date)[6:8]                                 #récupère le jour de la date
    if int(decade) == 11 or int(decade) == 1:               #si il s'agit de l'une des deux premières décades du mois
        for i in range(10):                                 #répète 10 fois
            cumul+=cumul_pluviométrique_jour(date)          #cumul d'un jour
            date+=1                                         #passe au jour suivant
    else:                                                   #cas de la 3ème décade à durée variable selon le mois
        month = str(date)[4:6]                              #récupère le mois
        year = str(date)[0:4]                               #récupère l'année
        if int(year) % 4 == 0:                              #vérifie si l'année est bissextile
            jours = mois_bissextile[month]                  #définit le nombre de jours du mois grace au dictionnaire des années bissextiles
        else:                                               #sinon
            jours = moiss[month]                            #définit le nombre de jours du mois grace au dictionnaire des années "classiques"
        jours= jours - 20                                   #enlève 20 jours pour avoir uniquement le nombre de jours de la dernière décade
        for i in range(jours):                              #pour chaque jour de la décade
            cumul+=cumul_pluviométrique_jour(date)          #cumul des valeur des jours
            date+=1                                         #passage au jour suivant
    return cumul                                            #retourne le cumul

############################################################## Nombre d'éléments dans une periode / utilitaires #############################################################

def annees(datedebut,datefin):                          ##calcule le nombre d'années entre 2 dates
    anneedebut = str(datedebut)[0:4]                        #récupère l'année de la date du début
    anneefin = str(datefin)[0:4]                            #récupère l'année de la date de fin
    annees = int(anneefin) - int(anneedebut)                #calcule la différence entre les 2
    return annees                                           #renvoie le nombre d'années entre 2 dates

def mois(datedebut,datefin):                            ##calcule le nombre de mois entre 2 dates
    anneedebut = str(datedebut)[0:4]                        #récupère l'année de la date du début
    anneefin = str(datefin)[0:4]                            #récupère l'année de la date de fin
    moisdebut = str(datedebut)[4:6]                         #récupère le mois de la date du début
    moisfin = str(datefin)[4:6]                             #récupère le mois de la date de fin
    annee = 12*(int(anneefin) - int(anneedebut))            #donne le nombre d'années puis x12 pour le nombre de mois
    mois = (int(moisfin) - int(moisdebut)) + annee          #fais la différence et y additione le nombre de mois pour les annnés en plus
    return mois                                             #renvoie le nombre de mois entre 2 dates

def saisons(datedebut,datefin):                         ##calcule le nombre de saisons entre 2 dates
    nbrsaisons = mois(datedebut,datefin) / 3                #calcule le nombre de mois et le divise par 3 pour avoir des saisons
    return nbrsaisons                                       #retourne la valeur

def numerosaisons(datedebut):
    saison=1                                            ##définit la saison à laquelle appartient une date
    if str(datedebut)[4:6] == '12':                         #si c'est le mois de décembre
        saison = 4                                          # 4 = hiver dans notre programme
    elif str(datedebut)[4:6] == '09':                       #si c'est le mois de septembre
        saison = 3                                          # 3 = automne dans notre programme
    elif str(datedebut)[4:6] == '06':                       #si c'est le mois de juin
        saison = 2                                          # 2 = été dans notre programme
    elif str(datedebut)[4:6] == '03':                       #si c'est le mois de mars
        saison = 1                                          # 1 = printemps dans notre programme
    return saison                                           #retourne la saison a partir du mois de départ de celle ci dans une date

def decades(datedebut,datefin):                         ##calcule le nombre de décades entre 2 dates
    debut = 0                                               #initialise la variable debut
    fin = 0                                                 #initialise la variable fin
    if str(datedebut)[6:8] == '21':                         #si on est au début de la 3ème décade
        debut = 1                                           #nombre de décades restantes pour atteindre le mois suivant
        if str(datedebut)[4:6] == '12':                     #si l'on est en décembre
            datedebut = int(datedebut) + 8880               #passe au premier janvier de l'année d'après
        else:                                               #sinon
            datedebut = int(datedebut) + 80                 #passe au premier du mois d'après
    elif str(datedebut)[6:8] == '11':                       # ou, si l'on est au début de la 2nde décade
        debut = 2                                           #nombre de décades restantes pour atteindre le mois suivant
        if str(datedebut)[4:6] == '12':                     #si l'on est en décembre
            datedebut = int(datedebut) + 8890               #passe au premier janvier de l'année d'après
        else:                                               #sinon
            datedebut = int(datedebut) + 90                 #passe au premier du mois suivant
    if str(datefin)[6:8] == '21':                           #si on est au jour 21
        fin = 2                                             #deux décades a la fin
        datefin = int(datefin) - 20                         #définit la date de fin au premier du mois
    elif str(datefin)[6:8] == '11':                         #si on est au jour 11
        fin = 1                                             #une décade a la fin
        datefin = int(datefin) - 10                         #définit la date de fin au premier du mois
    decades = mois(datedebut,datefin) *3 + debut + fin      #donne le nombre total de décades
    return decades                                          #retourne cette valeur

########################################################################## Graphiques #########################################################################

def cpag(datedebut,datefin):                                   ##Cumul Pluviométrique pour plusieurs années
    date = datedebut                                                    #définit la valeur de la date au début
    nbrannee = annees(datedebut,datefin)                                #calcule le nombre d'années entre 2 dates
    valeurs=[]                                                          #crée la liste des valeurs des ordonnéest
    x = []                                                              #initialise la liste des abscisses qui contiendra les années
    for i in range(nbrannee):                                           #répète pour le nombre d'années nécéssaires
        valeurs.append(round(cumul_pluvimetrique_annee(date),2))        #fais et ajoute le cumul de l'année à une liste
        x.append(str(date)[0:4])                                        #on ajoute l'année à x
        date+=10000                                                     #passe à l'année d'après
    plt.xticks(rotation=45)                                             #rotation des années de 45° sur l'axe x
    plt.plot(x, valeurs, color = 'b', label = 'test')                   #"construit" le graphique
    plt.title("Cumul pluviométrique par année")                         #titre du graphique
    plt.xlabel("Années")                                                #nom de l'axe x
    plt.ylabel("Cumul pluviométrique")                                  #nom de l'axe y
    plt.show()                                                          #affiche le graphique
    return valeurs

def cpmg(datedebut,datefin):                            ##Graphique du cumul pluviométrique pour plusieurs mois
    date = datedebut                                        #définit la date à la date de debut
    nmbrmois = mois(datedebut,datefin)                      #compte le nombre de mois entre 2 dates
    x0 = []                                                 #initialise une lise contenant les dates pour le graphique (format AAAAMMJJ)
    valeurs=[]                                              #initialise la liste contenant les valeurs
    for i in range(nmbrmois):                               #pour chaque mois à traiter
        valeurs.append(cumul_pluviometrique_mois(date))     #fais et ajoute le cumul du mois à une liste
        x0.append(date)                                     # on ajoute la date à x0
        if str(date)[4:6] == '12' :                         #si l'on traite le mois de décembre
            date+=8900                                      #passer à l'année d'après
        else:                                               #sinon
            date+=100                                       #passer au mois suivant
    x = []                                                  #initialise une liste qui contiendra les dates de x0 mais au format JJ/MM/AAAA
    for val in x0:                                          #pour chaque valeur de x0
        val = datetime.strptime(str(val), "%Y%m%d")         #transforme val en objet datetime pour pouvoir lui appliquer un format de date
        val = val.strftime("%d/%m/%Y")                      #transforme val au format JJ/MM/AAAA
        x.append(val)                                       #ajoute la date à la liste
    plt.xticks(rotation=45)                                 #rotation des années de 45° sur l'axe x
    plt.plot(x, valeurs, color = 'b', label = 'test')       #"construit" le graphique
    plt.title("Cumul pluviométrique par mois")              #titre du graphique
    plt.xlabel("Mois")                                      #nom de l'axe x
    plt.ylabel("Cumul pluviométrique")                      #nom de l'axe y
    plt.show()                                              #affiche le graphique
    return valeurs

def cpdg(datedebut,datefin):                             ##Graphique du cumul pluviométrique sur plusieurs décades
    date = datedebut                                        #définit la date à la date de début
    nbrdecades = decades(datedebut,datefin)                 #calcule le nombre de décades entre 2 dates
    valeurs=[]                                              #initialise la liste des ordonnées
    x = []                                                  #initialise la liste des abcisses
    for i in range(nbrdecades):                             #pour chaque décade entre les 2 dates
        valeurs.append(cumul_pluviometrique_decade(date))   #ajoute les valeurs à une liste
        if int(str(date)[6:8]) == 11:                       #si on est au début de la deuxième décade
            x.append(str(date)[4:6] + '/' + str(date)[0:4] + ' deuxième décade')    #ajoute a la liste des abcisses la date
            date += 10                                      #passe a la décade suivante
        elif int(str(date)[6:8])  == 1:                     #si on est le premier du mois
            x.append(str(date)[4:6] + '/' + str(date)[0:4] +  ' première décade')   #ajoute la date a liste des abcisses
            date += 10                                      #passe à la prochaine décade
        elif int(str(date)[6:8]) == 21:                     #si l'on est à la troisième décade
            if str(date)[4:6] == '12':                      #si on est en décembre
                x.append(str(date)[4:6] + '/' + str(date)[0:4] +  ' troisième décade')  #ajoute la date au abcisses
                date +=8880                                 #passe à l'année suivante
            else :                                          #sinon
                x.append(str(date)[4:6] + '/' + str(date)[0:4] +  ' troisième décade') #ajoute la date aux abcisses
                date += 80                                  #passe au mois suivant
    plt.xticks(rotation=45)                                 #rotation des années de 45° sur l'axe x
    plt.plot(x, valeurs, color = 'b', label = 'test')       #"construit" le graphique
    plt.title("Cumul pluviométrique par mois")              #titre du graphique
    plt.xlabel("Mois")                                      #nom de l'axe x
    plt.ylabel("Cumul pluviométrique")                      #nom de l'axe y
    plt.show()                                              #affiche le graphique
    return valeurs

def cpsg(datedebut,datefin):                            ##cumul pluviométrique sur plusieurs saisons (graphique)
    date = datedebut                                        #définit la date à la date de début
    nbrsaison = int(saisons(datedebut,datefin))             #calcule le nombre de saisons a entre les deux dates
    valeurs=[]                                              #crée la liste valeurs
    x = []                                                  #crée la liste des abcisses
    saison = numerosaisons(datedebut)                       #trouve a quelle saison appartient une date
    for i in range(nbrsaison):                              #pour chaque saison entre les dates
        valeurs.append(cumul_pluviometrique_saison(date))   #ajoute la valeur à la liste
        if saison == 4:                                     #si on est en hiver
            x.append('hiver ' + str(date)[0:4])             #ajoute a la liste des abcisses la date
            date += 9100                                    #passe a mars de l'année suivante (printemps)
            saison = 1                                      #définit la saison a printemps
        elif saison == 3:                                   #si la saison est l'automne
            x.append('automne ' + str(date)[0:4])           #ajoute la date aux abcisses
            date += 300                                     #passe la date à la saison suivante (Hiver)
            saison += 1                                     #passe à la saison suivante
        elif saison == 2:                                   #si la saison est l'été
            x.append('été ' + str(date)[0:4])               #ajoute la date aux abcisses
            date += 300                                     #passe la date à la saison suivante (Automne)
            saison += 1                                     #passe à la saison suivante
        elif saison == 1:                                   #si on est au printemps
            x.append('printemps ' + str(date)[0:4])         #ajoute la date aux abcisses
            date += 300                                     #passe la date à la saison suivante (été)
            saison += 1                                     #passe à la saison suivante
    plt.xticks(rotation=45)                                 #rotation des années de 45° sur l'axe x
    plt.plot(x, valeurs, color = 'b', label = 'test')       #"construit" le graphique
    plt.title("Cumul pluviométrique par saison")            #titre du graphique
    plt.xlabel("Mois")                                      #nom de l'axe x
    plt.ylabel("Cumul pluviométrique")                      #nom de l'axe y
    plt.show()                                              #affiche le graphique
    return valeurs

def cpjg(datedebut,datefin):                            ##graphique pluviométrique par jour sur une durée
    date = datedebut                                        #initialise la date à la date du début
    valeurs = []                                            #initialise la liste des valeurs
    x0 = []                                                 #initilaise la liste des dates
    for i in range(trouver_ligne(datefin) - trouver_ligne(datedebut)): #pour chaque valeur du tableau excel entre les deux dates
        valeurs.append(cumul_pluviométrique_jour(date))     # on ajoute le cumul du jour à la liste des valeurs
        x0.append(date)                                     # on ajoute la date à la liste des dates
        month = str(date)[4:6]                              # on trouve le mois
        jour = str(date)[6:8]                               # on trouve le jour
        annee = str(date)[0:4]                              # on trouve l'année
        if month == '12' and jour == '31':                  # si on est au dernier jours de l'année
            date += 8870                                    # on passe à l'année suivante
        elif bissextile(int(annee)) == True:                # si l'année est bissextile
            if int(jour) == mois_bissextile[month]:         # si on est à la fin du mois
                date += (100 - int(jour) + 1)               # on passe au mois suivant
            else:
                date += 1                                   # sinon on ajoute 1
        elif bissextile(int(annee)) == False:               # idem si l'année n'est pas bissextile
            if int(jour) == moiss[month]:
                date += (100 - int(jour) + 1)
            else:
                date += 1
    x = []                                                  #initialise une liste qui contiendra les dates de x0 mais au format JJ/MM/AAAA
    for val in x0:                                          #pour chaque valeur de x0
        val = datetime.strptime(str(val), "%Y%m%d")         # transforme val en objet datetime pour pouvoir lui appliquer un format de date
        val = val.strftime("%d/%m/%Y")                      # transforme val au format JJ/MM/AAAA
        x.append(val)
    plt.xticks(rotation=45)                                 #rotation des années de 45° sur l'axe x
    plt.plot(x, valeurs, color = 'b', label = 'test')       #"construit" le graphique
    plt.title("Cumul pluviométrique par jour")              #titre du graphique
    plt.xlabel("Mois")                                      #nom de l'axe x
    plt.ylabel("Cumul pluviométrique")                      #nom de l'axe y
    plt.show()
    return valeurs

############################################################### fin des fonctions, début de l'interface ############################################################################

fenetre = tk.Tk()                                                                                   #on créé la fenêtre
fenetre.title("CumuloGraphe")                                                        #donne le nom de la fenêtre
fenetre.geometry("800x600")                                                                         #taille de la fenêtre au lancement du programme

label = tk.Label(fenetre, text="Bonjour, grâce à cette application, vous allez pouvoir faire différents graphiques sur le cumul pluviométrique de l'Alsace de 1923 à 2023")                    #donne un texte dans l'interface
label.pack()                                                                                       #fait apparaitre le texte dans la fenêtre
label_1 = tk.Label(fenetre, text="cliquez ici pour ouvrir un fichier :")                    #donne un texte dans l'interface
label_1.pack(pady=20)                                                                       #définit l'espacement entre le haut et le bas du texte
df = ''         #définit la variable qui accueillera le fichier excel


anneesmenus=list(range(1923, 2024))                                             #créé une liste d'années de 1923 à 2023 qui servira pour les menus déroulant
saisonsmenus=[1,2,3,4]                                                          #créé une liste cmprenant les 4 saisons avec 1 pour le printemps etc... pour les menus déroulant
moismenus = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]                             #créé une liste avec les 12 mois de l'année pour les menus déroulant
decadesmenus=[1,2,3]                                                            #créé une liste avec les 3 décades de chaque mois pour les menus déroulant
joursmenus=list(range(1,32))                                                    #créé une liste avec les 31 jours des mois pour les menus déroulant

anneedebut=0    #définit anneedebut à 0
moisdebut=0     #définit moisdebut à 0
jourdebut=0     #définit jourdebut à 0
anneefin=0      #définit anneefin à 0
moisfin=0       #définit moisfin à 0
jourfin=0       #définit jourfin à 0
datedebut=0     #définit datedebut à 0
datefin=0       #définit datefin à 0


#créé la fonction Open_F qui va permettre de choisir un excel dans nos dossier
def Open_F():
    global df
    df= pd.read_excel(askopenfilename(initialdir=r"C:\Desktop", title="Ouvrir"))    #permet d'ouvrir les documents afin de choisir un fichier excel et le stock dans la variable df

#bouton qui permet dappeler la fonction open_f et donc de choisir un fichier dans les dossiers de l'ordinateur
bouton = tk.Button(fenetre, text="Ouvrir fichier", fg="blue", command=Open_F)
bouton.pack()   #permet d'afficher le bouton

#affiche un petit texte donnant des indications pour la saisie des dates
label_2 = tk.Label(fenetre, text="\nVeuillez renseigner l'option que vous voulez pour vos graphiques et veillez bien à renseigner toutes les dates demandées. \n\n PS: dans le fichier excel avec les données météorologiques de Strasbourg que nous fournissons avec notre projet, \nil est important de noter que les données de l'année 1923 commencent en mai et que nous n'avons pas de données du 31 août 1939 au 1er avril 1941, \n ainsi que du 22 novembre 1944 au 1er juin 1945, donc si vous incluez ces valeurs le programme enverra une erreur.")     #donne un texte dans l'interface
label_2.pack(pady=10)       #affiche le texte avec un espacement de 10 pixel en haut et en bas du texte

label_annee=tk.Label(fenetre, text="vous avez choisi de faire un graphique en faisant des moyennes du cumul pluviométrique par années.")    #créé un texte qui s'affichera si le bouton année est cliqué
label_saisons=tk.Label(fenetre, text="vous avez choisi de faire un graphique en faisant des moyennes du cumul pluviométrique par saisons")  #créé un texte qui s'affichera si le bouton saisons est cliqué
label_mois=tk.Label(fenetre, text="vous avez choisi de faire un graphique en faisant des moyennes du cumul pluviométrique par mois")    #créé un texte qui s'affichera si le bouton mois est cliqué
label_decade=tk.Label(fenetre, text="vous avez choisi de faire un graphique en faisant des moyennes du cumul pluviométrique par décades")   #créé un texte qui s'affichera si le bouton décade est cliqué
label_jour=tk.Label(fenetre, text="vous avez choisi de faire un graphique du cumul pluviométrique par jour")    #créé un texte qui s'affichera si le bouton jour est cliqué

def isNumeric(s):       #créé une fonction qui renvoie True si il s'agit d'un nombre entier et sinon renvoie False
    try:        #fait un essaie
        int(s)      #test si "s" est un nombre entier
        return True
    except ValueError:
        return False

bouton2 = tk.Button(fenetre, text="faire le graphique", fg="orange", command=lambda: cpag(datedebut, datefin))  #créé le bouton qui s'affichera si le bouton année est cliqué et que toutes les valeurs permettant de donné la date sont rempli
bouton3 = tk.Button(fenetre, text="faire le graphique", fg="orange", command=lambda: cpsg(datedebut, datefin))  #créé le bouton qui s'affichera si le bouton saisons est cliqué et que toutes les valeurs permettant de donné la date sont rempli
bouton4 = tk.Button(fenetre, text="faire le graphique", fg="orange", command=lambda: cpmg(datedebut, datefin))  #créé le bouton qui s'affichera si le bouton mois est cliqué et que toutes les valeurs permettant de donné la date sont rempli
bouton5 = tk.Button(fenetre, text="faire le graphique", fg="orange", command=lambda: cpdg(datedebut, datefin))  #créé le bouton qui s'affichera si le bouton décades est cliqué et que toutes les valeurs permettant de donné la date sont rempli
bouton6 = tk.Button(fenetre, text="faire le graphique", fg="orange", command=lambda: cpjg(datedebut, datefin))  #créé le bouton qui s'affichera si le bouton jour est cliqué et que toutes les valeurs permettant de donné la date sont rempli

##ici commence les boutons
def afficher_selection(*args):
    global anneedebut,anneefin,moisdebut,moisfin,jourdebut,jourfin,datedebut,datefin    #permet que les variables définit puissent être utilisé dans cette fonction
    # Masquer tous les éléments d'affichage afin que lorsque l'utilisateur change de bouton année, saisons, mois, décades, jour, les boutons qui s'affichent sont bien ceux du bon bouton
    dropdown_annee_debut.pack_forget()
    dropdown_saisons_debut.pack_forget()
    dropdown_mois_debut.pack_forget()
    dropdown_decades_debut.pack_forget()
    dropdown_jour_debut.pack_forget()
    dropdown_annee_fin.pack_forget()
    dropdown_saisons_fin.pack_forget()
    dropdown_mois_fin.pack_forget()
    dropdown_decades_fin.pack_forget()
    dropdown_jour_fin.pack_forget()
    bouton2.pack_forget()
    bouton3.pack_forget()
    bouton4.pack_forget()
    bouton5.pack_forget()
    bouton6.pack_forget()
    label_annee.pack_forget()
    label_saisons.pack_forget()
    label_mois.pack_forget()
    label_decade.pack_forget()
    label_jour.pack_forget()

    # Afficher les éléments correspondants à l'option sélectionnée
    selection = selected_option.get()
    if selection == "Année":            #si le bouton sélectionné est année
        label_annee.pack(pady=5)        #affiche le texte label_annee

        #affiche les différents menus dérulant dont nous avons besoin pour déterminer la date
        dropdown_annee_debut.pack(pady=10)
        dropdown_annee_fin.pack(pady=10)

        if isNumeric(dropdown_annee_debut.get()) and isNumeric(dropdown_annee_fin.get())==True : #si les deux menus déroulants ont une valeur en nombre entier
            anneedebut = dropdown_annee_debut.get()     #anneedebut prend la valeur qu'il y a dans le menus déroulant
            moisdebut = 1       #comme c'est par année le jour et le mois seront directement le 1er janvier
            jourdebut = 1
            anneefin = dropdown_annee_fin.get()     #anneefin prend la valeur qu'il y a dans le menus déroulant
            moisfin = 1     #comme c'est par année le jour et le mois seront directement le 1er janvier
            jourfin = 1
            datedebut=int(anneedebut)*10000+int(moisdebut)*100+int(jourdebut)       #ca permet d'avoir datedebut sous forme AAAAMMJJ
            datefin=int(anneefin)*10000+int(moisfin)*100+int(jourfin)       #ca permet d'avoir datefin sous forme AAAAMMJJ

            bouton2.pack()      #affiche le bouton qui va pouvoir faire les graphiques

    elif selection == "Saison":     #si le bouton sélectionné est saison
        label_saisons.pack(pady=5)  #affiche le texte label_saisons

        #affiche les différents menus dérulant dont nous avons besoin pour déterminer la date
        dropdown_annee_debut.pack(pady=10)
        dropdown_saisons_debut.pack(pady=10)
        dropdown_annee_fin.pack(pady=10)
        dropdown_saisons_fin.pack(pady=10)

        if isNumeric(dropdown_annee_debut.get()) and isNumeric(dropdown_annee_fin.get()) and isNumeric(dropdown_saisons_fin.get()) and isNumeric(dropdown_saisons_debut.get())==True : #si les quatres menus déroulants ont une valeur en nombre entier

            anneedebut = dropdown_annee_debut.get()     #anneedebut prend la valeur qu'il y a dans le menus déroulant
            if dropdown_saisons_debut.get()=="1":       #si la saisons de debut est le printemps le mois de debut sera 3
                moisdebut=3
            elif dropdown_saisons_debut.get()=="2":     #si la saisons de debut est l'été le mois de debut sera juin
                moisdebut=6
            elif dropdown_saisons_debut.get()=="3":     #si la saisons de debut est l'automne le mois de debut sera septembre
                moisdebut=9
            elif dropdown_saisons_debut.get()=="4":     #si la saisons de debut est l'hiver le mois de debut sera decembre
                moisdebut=12
            jourdebut = 1       #comme la récurrence est les saisons le jour de debut sera le premier du mois
            anneefin = dropdown_annee_fin.get()     #anneefin prend la valeur qu'il y a dans le menus déroulant
            if dropdown_saisons_fin.get()=="1":     #si la saisons de fin est le printemps le mois de debut sera 3
                moisfin=3
            elif dropdown_saisons_fin.get()=="2":       #si la saisons de fin est l'été le mois de debut sera juin
                moisfin=6
            elif dropdown_saisons_fin.get()=="3":       #si la saisons de fin est l'automne le mois de debut sera septembre
                moisfin=9
            elif dropdown_saisons_fin.get()=="4":       #si la saisons de fin est l'hiver le mois de debut sera decembre
                moisfin=12
                jourfin = 1
            datedebut=int(anneedebut)*10000+int(moisdebut)*100+int(jourdebut)       #ca d'avoir datedebut sous forme AAAAMMJJ
            datefin=int(anneefin)*10000+int(moisfin)*100+int(jourfin)       #ca permet d'avoir datefin sous forme AAAAMMJJ
            print(datedebut)
            bouton3.pack()      #affiche le bouton qui va pouvoir faire les graphiques

    elif selection == "Mois":       #si le bouton sélectionné est mois
        label_mois.pack(pady=5) #affiche le texte label_mois

        #affiche les différents menus dérulant dont nous avons besoin pour déterminer la date
        dropdown_annee_debut.pack(pady=10)
        dropdown_mois_debut.pack(pady=10)
        dropdown_annee_fin.pack(pady=10)
        dropdown_mois_fin.pack(pady=10)

        if isNumeric(dropdown_annee_debut.get()) and isNumeric(dropdown_annee_fin.get()) and isNumeric(dropdown_mois_fin.get()) and isNumeric(dropdown_mois_debut.get())==True :    #si les quatres menus déroulants ont une valeur en nombre entier
            anneedebut = dropdown_annee_debut.get()     #anneedebut prend la valeur qu'il y a dans le menus déroulant
            moisdebut = dropdown_mois_debut.get()       #moisdebut prend la valeur qu'il y a dans le menus  déroulant
            jourdebut = 1           #comme c'est par mois, le jour de debut sera le premier jour du mois
            anneefin = dropdown_annee_fin.get()     #anneefin prend la valeur qu'il y a dans le menus déroulant
            moisfin = dropdown_mois_fin.get()       #moisfin prend la valeur qu'il y a dans le menus déroulant
            jourfin = 1     #comme c'est par mois, le jour de fin sera le premier jour du mois
            datedebut=int(anneedebut)*10000+int(moisdebut)*100+int(jourdebut)        #ca permet d'avoir datedebut sous forme AAAAMMJJ
            datefin=int(anneefin)*10000+int(moisfin)*100+int(jourfin)       #ca permet d'avoir datefin sous forme AAAAMMJJ

            bouton4.pack()      #affiche le bouton qui va pouvoir faire les graphiques

    elif selection == "Décade":     #si le bouton sélectionné est décade
        label_decade.pack(pady=5)       #affiche le texte label_decade

        #affiche les différents menus dérulant dont nous avons besoin pour déterminer la date
        dropdown_annee_debut.pack(pady=10)
        dropdown_mois_debut.pack(pady=10)
        dropdown_decades_debut.pack(pady=10)
        dropdown_annee_fin.pack(pady=10)
        dropdown_mois_fin.pack(pady=10)
        dropdown_decades_fin.pack(pady=10)

        if isNumeric(dropdown_annee_debut.get()) and isNumeric(dropdown_annee_fin.get()) and isNumeric(dropdown_mois_fin.get()) and isNumeric(dropdown_mois_debut.get()) and isNumeric(dropdown_decades_fin.get()) and isNumeric(dropdown_decades_debut.get())==True :  #si les six menus déroulants ont une valeur en nombre entier
            anneedebut = dropdown_annee_debut.get()     #anneedebut prend la valeur qu'il y a dans le menus déroulant
            moisdebut = dropdown_mois_debut.get()       #moisdebut prend la valeur qu'il y a dans le menus  déroulant
            if dropdown_decades_debut.get()=="1":       #si la décade de debut est la premiere le jour de debut sera 1
                jourdebut=1
            elif dropdown_decades_debut.get()=="2":     #si la décade de debut est la deuxieme, le jour de debut sera 11
                jourdebut=11
            elif dropdown_decades_debut.get()=="3":     #si la décade de debut est la troisieme, le jour de debut sera 21
                jourdebut=21
            anneefin = dropdown_annee_fin.get()     #anneefin prend la valeur qu'il y a dans le menus déroulant
            moisfin = dropdown_mois_fin.get()       #moisfin prend la valeur qu'il y a dans le menus déroulant
            if dropdown_decades_fin.get()=="1":     #si la décade de fin est la premiere le jour de fin sera 11
                jourfin=11
            elif dropdown_decades_fin.get()=="2":       #si la décade de fin est la deuxieme le jour de fin sera 21
                jourfin=21
            elif dropdown_decades_fin.get()=="3":       #si la décade de fin est la troisieme le jour de fin sera :
                if moisfin ==1 or moisfin ==3 or moisfin ==5 or moisfin ==7 or moisfin ==8 or moisfin ==10 or moisfin ==12:  #31 si le mois est un mois à 31 jours
                    jourfin=31
                elif moisfin ==4 or moisfin ==6 or moisfin ==9 or moisfin ==11:     #30 si le mois est un mois à 30 jours
                    jourfin=30
                elif moisfin==2 :       #et si cest février
                    if anneefin%4==0:       #si c'est une annee bissextile le jour de fin sera 29
                        jourfin=29
                    else:
                        jourfin=28      #sinon ce sera 28

            datedebut=int(anneedebut)*10000+int(moisdebut)*100+int(jourdebut)        #ca permet d'avoir datedebut sous forme AAAAMMJJ
            datefin=int(anneefin)*10000+int(moisfin)*100+int(jourfin)       #ca permet d'avoir datefin sous forme AAAAMMJJ
            bouton5.pack()      #affiche le bouton qui va pouvoir faire les graphiques

    elif selection == "Jour":       #si le bouton sélectionné est jour
        label_jour.pack(pady=5)     #affiche le texte label_decade

        #affiche les différents menus dérulant dont nous avons besoin pour déterminer la date
        dropdown_annee_debut.pack(pady=10)
        dropdown_mois_debut.pack(pady=10)
        dropdown_jour_debut.pack(pady=10)
        dropdown_annee_fin.pack(pady=10)
        dropdown_mois_fin.pack(pady=10)
        dropdown_jour_fin.pack(pady=10)

        if isNumeric(dropdown_annee_debut.get()) and isNumeric(dropdown_annee_fin.get()) and isNumeric(dropdown_mois_fin.get()) and isNumeric(dropdown_mois_debut.get()) and isNumeric(dropdown_jour_fin.get()) and isNumeric(dropdown_jour_debut.get())==True :        #si les six menus déroulants ont une valeur en nombre entier
            #récupère les valeurs des menus deroulant pour les mettre dans les variables
            anneedebut = dropdown_annee_debut.get()
            moisdebut = dropdown_mois_debut.get()
            jourdebut = dropdown_jour_debut.get()
            anneefin = dropdown_annee_fin.get()
            moisfin = dropdown_mois_fin.get()
            jourfin = dropdown_jour_fin.get()
            datedebut=int(anneedebut)*10000+int(moisdebut)*100+int(jourdebut)        #ca permet d'avoir datedebut sous forme AAAAMMJJ
            datefin=int(anneefin)*10000+int(moisfin)*100+int(jourfin)       #ca permet d'avoir datefin sous forme AAAAMMJJ

            bouton6.pack()      #affiche le bouton qui va pouvoir faire les graphiques

    return datedebut,datefin        #renvoie la date de debut et la date de fin

selected_option = tk.StringVar()        #permet de rendre dynamique les boutons
selected_option.trace("w", afficher_selection)

# Création d'un style personnalisé pour les boutons radio
style = ttk.Style()
style.configure("Custom.TRadiobutton", indicatorsize=20)

# Création d'un Frame pour contenir les boutons radio
frame_radios = ttk.Frame(fenetre)
frame_radios.pack(side="top", pady=15)

# Boutons radio personnalisés pour les options Année, Mois, Jour
options = ["Année", "Saison", "Mois", "Décade", "Jour"]
for option in options:
    ttk.Radiobutton(frame_radios, text=option, variable=selected_option, value=option, style="Custom.TRadiobutton").pack(side="left", padx=10)

##ici termine les boutons

################################################################### création des menus déroulant #######################################################################
dropdown_annee_debut = ttk.Combobox(fenetre, values=anneesmenus, state="readonly")      #créé un menus déroulant avec les valeurs des années de 1923 à 2023
dropdown_annee_debut.set("annee de début")      #donne le nom du menus déroulant

dropdown_saisons_debut = ttk.Combobox(fenetre, values=saisonsmenus, state="readonly")   #créé un menus déroulant avec les valeurs des saisons
dropdown_saisons_debut.set("saisons de début")      #donne le nom du menus déroulant

dropdown_mois_debut = ttk.Combobox(fenetre, values=moismenus, state="readonly")     #créé un menus déroulant avec les valeurs des mois
dropdown_mois_debut.set("mois de début")        #donne le nom du menus déroulant

dropdown_decades_debut = ttk.Combobox(fenetre, values=decadesmenus, state="readonly")       #créé un menus déroulant avec les valeurs des décades
dropdown_decades_debut.set("décades de début")      #donne le nom du menus déroulant

dropdown_jour_debut = ttk.Combobox(fenetre, values=joursmenus, state="readonly")        #créé un menus déroulant avec les valeurs des jours
dropdown_jour_debut.set("jour de début")        #donne le nom du menus déroulant

dropdown_annee_fin = ttk.Combobox(fenetre, values=anneesmenus, state="readonly")        #créé un menus déroulant avec les valeurs des années de 1923 à 2023
dropdown_annee_fin.set("année de fin")      #donne le nom du menus déroulant

dropdown_saisons_fin = ttk.Combobox(fenetre, values=saisonsmenus, state="readonly")     #créé un menus déroulant avec les valeurs des saisons
dropdown_saisons_fin.set("saisons de fin")      #donne le nom du menus déroulant

dropdown_mois_fin = ttk.Combobox(fenetre, values=moismenus, state="readonly")       #créé un menus déroulant avec les valeurs des mois
dropdown_mois_fin.set("mois de fin")        #donne le nom du menus déroulant

dropdown_decades_fin = ttk.Combobox(fenetre, values=decadesmenus, state="readonly")     #créé un menus déroulant avec les valeurs des décades
dropdown_decades_fin.set("décades de fin")      #donne le nom du menus déroulant

dropdown_jour_fin = ttk.Combobox(fenetre, values=joursmenus, state="readonly")      #créé un menus déroulant avec les valeurs des jours
dropdown_jour_fin.set("jour de fin")        #donne le nom du menus déroulant
############################################################ fin de la création des menus déroulant ######################################################################

fenetre.mainloop()      #permet de de rafraichir la page de manière permanente